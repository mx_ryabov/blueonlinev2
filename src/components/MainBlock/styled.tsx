import styled from 'styled-components';
import { GhostButton } from '../ui';
import SquareIcon from '../../assets/imgs/square-icon.svg';
import StarIcon from '../../assets/imgs/star-icon.svg';
import MouseIcon from '../../assets/imgs/mouse-icon.svg';

export const Title = styled.h1`
    line-height: 90%;
    margin-bottom: 20px;
    margin-top: 0;
    font-family: 'Kontora ExtraBold';
    max-width: 350px;
    margin-left: auto;
    margin-right: auto;

    span {
        font-family: 'Kontora Light';
        color: #072847;
        font-size: 21px;
    }

    @media only screen and (min-width: 700px) {
        font-size: 34px;
        margin-left: 0;
        margin-right: 0;
        span {
            font-size: 25px;
        }
    }
    @media only screen and (min-width: 1024px) {
        font-size: 42px;
        max-width: 606px;
        span {
            font-size: 32px;
        }
    }
    @media only screen and (min-width: 1440px) {
        font-size: 52px;
        span {
            font-size: 40px;
        }
    }
`;

export const MainBlockContainer = styled.div`
    padding: 15px 25px 40px;

    p {
        font-size: 16px;
        margin-bottom: 20px;
        max-width: 350px;
        margin-left: auto;
        margin-right: auto;
    }
    img {
        display: block;
        max-width: 310px;
        max-height: 296px;
        margin: 0 auto 25px;
    }
    a {
        margin: 0 auto;
    }
    ${GhostButton}, .description {
        display: none;
    }

    @media only screen and (min-width: 700px) {
        position: relative;
        padding: 25px 45px;
        p {
            margin: 0 0 40px 0;
        }

        img {
            position: absolute;
            top: 0;
            right: 5%;
            margin: 0;
            max-width: 502px;
            max-height: 481px;
            width: 40%;
        }
        a {
            margin: 0;
        }
    }

    @media only screen and (min-width: 1024px) {
        .buttons {
            display: flex;
            flex-direction: row;
            margin-bottom: 90px;
        }
        ${GhostButton} {
            display: flex;
            margin-left: 30px;
        }
        .description {
            display: flex;
            flex-direction: row;
            width: 65%;
            max-width: 660px;

            & > div {
                font-weight: 500;
                font-size: 14px;
                line-height: 160%;
                color: #2e2e2e;
                padding-left: 45px;
                position: relative;

                &::before {
                    content: '';
                    display: block;
                    width: 24px;
                    height: 24px;
                    position: absolute;
                    left: 0;
                    top: 20px;
                }
            }
            &-first {
                margin-right: 40px;
            }

            &-first::before {
                background: url(${SquareIcon}) 0 0 no-repeat;
            }
            &-second::before {
                background: url(${StarIcon}) 0 0 no-repeat;
            }
        }
        p {
            font-size: 18px;
        }
    }

    @media only screen and (min-width: 1440px) {
        padding: 0 120px 0 163px;
        position: relative;
        max-width: 1440px;
        margin: 60px auto 70px;

        &::before {
            content: '';
            display: block;
            width: 1px;
            height: 100%;
            position: absolute;
            left: 65px;
            top: 0;
            background-color: #e0e0e0;
        }
        &::after {
            content: '';
            display: block;
            width: 60px;
            height: 60px;
            position: absolute;
            left: 35px;
            bottom: 100px;
            background: center no-repeat url(${MouseIcon}), #fff 0 0;
        }
        p {
            font-size: 20px;
        }
    }
`;
