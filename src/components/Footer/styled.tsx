import styled from 'styled-components';
import MailIcon from '../../assets/imgs/mail-icon.svg';
import PhoneIcon from '../../assets/imgs/phone-icon.svg';

export const FooterContainer = styled.footer`
    background: #f9fbfc;
    padding: 45px 25px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media only screen and (min-width: 768px) {
        padding: 60px 45px;
    }
    @media only screen and (min-width: 1440px) {
        padding: 60px 65px;
    }
`;

export const InfoBox = styled.div`
    display: flex;
    flex-direction: column;

    span,
    p {
        color: #072847;
        font-weight: 600;
        line-height: 140%;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
    }
    span.email {
        font-size: 14px;
        margin-bottom: 5px;

        &::before {
            content: '';
            margin-right: 12px;
            width: 24px;
            height: 24px;
            background: url(${MailIcon}) 0 0 no-repeat;
        }
    }
    p.phone {
        font-size: 22px;
        margin: 0;

        &::before {
            content: '';
            width: 24px;
            height: 24px;
            margin-right: 10px;
            background: url(${PhoneIcon}) 0 0 no-repeat;
        }
    }

    @media only screen and (min-width: 768px) {
        span.email {
            font-size: 16px;
        }
        p.phone {
            font-size: 24px;
        }
    }
`;

export const Link = styled.a`
    font-weight: 600;
    font-size: 15px;
    line-height: 15px;
    color: #2e2e2e;
`;

export const LinkBox = styled.div`
    display: none;
    @media only screen and (min-width: 1440px) {
        display: flex;
        width: 300px;
        flex-direction: row;
        flex-wrap: wrap;
        position: absolute;
        left: 233px;

        ${Link} {
            width: 50%;
            margin-bottom: 15px;
        }
    }
`;
