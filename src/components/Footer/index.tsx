import React from 'react';
import { Logo } from '../ui';
import { FooterContainer, InfoBox, LinkBox, Link } from './styled';

export const Footer = () => {
    return (
        <FooterContainer>
            <Logo isWhite={false} />
            <LinkBox>
                <Link href="#our-services">Our services</Link>
                <Link href="#about">About</Link>
                <Link href="#cases">Cases</Link>
                <Link href="#contact-us">Contact Us</Link>
            </LinkBox>
            <InfoBox>
                <span className="email">info.neamob@gmail.com</span>
                <p className="phone">+972 52 8515949</p>
            </InfoBox>
        </FooterContainer>
    );
};
