import * as React from 'react';
import { SERVICES, ServiceType } from './data';
import { ServiceItem } from './ServiceItem';
import { OurServicesContainer, ServicesList } from './styled';

export default function OurServices() {
    return (
        <OurServicesContainer id="our-services">
            <h2>Our services</h2>
            <ServicesList>
                {SERVICES.map((ser: ServiceType, ind: number) => {
                    return <ServiceItem {...ser} key={ind} />;
                })}
            </ServicesList>
        </OurServicesContainer>
    );
}
