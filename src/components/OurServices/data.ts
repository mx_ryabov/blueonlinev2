export type ServiceType = {
    title: string;
    description: string;
    icon: string;
};
export const SERVICES: ServiceType[] = [
    {
        title: 'Online Strategy',
        description: 'Сreate a strategy that will allow to grow revenues and build all the structure around that goal',
        icon: 'online-strategy',
    },
    {
        title: 'Marketing',
        description: 'Drive high quality traffic and grow the business revenues',
        icon: 'marketing',
    },
    {
        title: 'Brand Design',
        description: 'Design a powerful online brand that attracts new users using a strong value proposition',
        icon: 'brand-design',
    },
    {
        title: 'UX/UI & Development',
        description: 'Build an easy to use website to convert users to become potential clients depending on the goals',
        icon: 'ui-ux',
    },
    {
        title: 'Analysis & Optimization',
        description:
            'Build strong reporting systems that will allow to analyze results and make good calculated decision toward the targets',
        icon: 'analysis-optimization',
    },
];
