import styled from 'styled-components';

export const OurServicesContainer = styled.div`
    background: #f9fbfc;
    padding: 30px 25px 50px;
    display: flex;
    flex-direction: column;

    @media only screen and (min-width: 768px) {
        padding: 30px 45px 45px;
    }
    @media only screen and (min-width: 1440px) {
        margin-left: calc(50% - 652px);
        padding: 40px 88px 55px 90px;
        border-radius: 70px 0px 0px 70px;
    }
`;

export const ServicesList = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
`;

export const ServiceBox = styled.div`
    padding-left: 80px;
    width: 100%;
    position: relative;
    margin-bottom: 40px;

    h3 {
        font-weight: 800;
        font-size: 20px;
        line-height: 140%;
        margin-top: 0;
        margin-bottom: 7px;
        color: #222222;
    }
    p {
        font-weight: 500;
        font-size: 14px;
        line-height: 140%;
        color: #2e2e2e;
        margin: 0;
    }

    @media only screen and (min-width: 700px) {
        padding-left: 0;
        width: 50%;

        &:first-of-type {
            padding-right: 30px;
        }
    }
    @media only screen and (min-width: 1024px) {
        width: 33.33%;
        &:not(:nth-child(3n)) {
            padding-right: 30px;
        }
    }
    @media only screen and (min-width: 1440px) {
        width: 20%;
        max-width: 168px;
        box-sizing: content-box;

        &:nth-child(n) {
            padding-right: 68px;
        }

        &::before {
            content: '';
            display: block;
            height: 1px;
            width: 112px;
            background-color: #91a8ca;
            position: absolute;
            left: 90px;
            top: 29px;
        }
    }
`;

export const ServiceIcon = styled.div<{ name: string }>`
    width: 58px;
    height: 58px;
    position: absolute;
    top: 0;
    left: 0;

    &::before,
    &::after {
        content: '';
        display: block;
        width: 58px;
        height: 58px;
        border-radius: 10px;
        position: absolute;
    }

    &::before {
        background: #656666;
        opacity: 0.1;
        filter: blur(35px);
        top: 6px;
        left: 6px;
        z-index: 0;
    }
    &::after {
        background: center no-repeat url(${(props) => require(`../../assets/imgs/${props.name}.svg`)}), #fff 0 0;
        z-index: 1;
        top: 0;
        left: 0;
    }

    @media only screen and (min-width: 700px) {
        position: relative;
        margin-bottom: 20px;
    }
`;
