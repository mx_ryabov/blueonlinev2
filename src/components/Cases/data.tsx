export type CaseType = {
    title: string;
    description: string;
    imageName: string;
};

export const CASE_ITEMS: CaseType[] = [
    { title: '0 to $100K', description: 'daily spend with ROAS above 2.0', imageName: 'daily-spend-case' },
    {
        title: '50% decrease',
        description: 'in cost per acquisition, and increase budget x2',
        imageName: 'in-cost-per-case',
    },
    { title: '150% increase', description: 'in revenue', imageName: 'in-revenues-case' },
];
