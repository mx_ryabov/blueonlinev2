import React from 'react';
import { CaseType } from './data';
import { CaseBox, Title, Description } from './styled';

interface Props extends CaseType {}

export const CaseItem = (props: Props) => {
    return (
        <CaseBox>
            <Title>{props.title}</Title>
            <Description>{props.description}</Description>
            <img src={require(`../../assets/imgs/${props.imageName}.jpg`)} alt="" />
        </CaseBox>
    );
};
