import styled from 'styled-components';

export const CheckboxWrapper = styled.label`
    display: flex;
    flex-direction: row;
    margin-bottom: 30px;
    position: relative;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;

    span {
        font-weight: 600;
        font-size: 14px;
        line-height: 140%;
        color: #2e2e2e;
        margin-left: 30px;
        max-width: 211px;
    }
    input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;

        &:checked ~ .checkmark {
            background-color: #fff;
        }
        &:checked ~ .checkmark:after {
            display: block;
        }
    }
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 18px;
        width: 18px;
        background-color: #fff;
        border: 1px solid #072847;
        border-radius: 3px;

        &:after {
            content: '';
            position: absolute;
            display: none;
            left: 5px;
            top: 2px;
            width: 4px;
            height: 8px;
            border: solid #072847;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    }
`;

export const ErrorMessage = styled.div`
    color: #fdaf26;
    font-size: 13px;
    position: absolute;
    bottom: -20px;
    left: 30px;
`;
