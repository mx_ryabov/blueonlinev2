import styled from 'styled-components';
import BurgerIcon from '../../assets/imgs/burger.svg';
import CloseIcon from '../../assets/imgs/close-icon.svg';
import MailIcon from '../../assets/imgs/white-mail-icon.svg';
import PhoneIcon from '../../assets/imgs/white-phone-icon.svg';

export const MenuContainer = styled.header`
    height: 105px;
    padding: 0 25px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    z-index: 10;
    width: 100%;

    & > a {
        z-index: 5;
    }

    @media only screen and (min-width: 700px) {
        padding: 0 45px;
    }
    @media only screen and (min-width: 1024px) {
        padding: 0 0 0 45px;
    }
    @media only screen and (min-width: 1440px) {
        padding: 0 calc(50% - 720px) 0 calc(50% - 652px);
    }
`;

export const MenuBurger = styled.div<{ isMenuOpen: boolean }>`
    width: 32px;
    height: 32px;
    z-index: 5;
    cursor: pointer;
    background: url(${(props) => (props.isMenuOpen ? CloseIcon : BurgerIcon)}) 0 0 no-repeat;

    @media only screen and (min-width: 1024px) {
        display: none;
    }
`;

export const InfoBox = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    position: absolute;
    bottom: 0;
    padding-bottom: 80px;

    span,
    p {
        color: #fff;
        font-weight: 600;
        line-height: 140%;
        display: flex;
        flex-direction: row;
        align-items: center;
    }
    span.email {
        font-size: 16px;
        margin-bottom: 16px;

        &::before {
            content: '';
            margin-right: 12px;
            width: 24px;
            height: 24px;
            background: url(${MailIcon}) 0 0 no-repeat;
        }
    }
    p.phone {
        font-size: 24px;
        margin: 0;

        &::before {
            content: '';
            margin-right: 15px;
            width: 24px;
            height: 24px;
            background: url(${PhoneIcon}) 0 0 no-repeat;
        }
    }
`;

export const MenuItem = styled.li<{ active?: boolean }>`
    margin-bottom: 44px;

    a {
        color: #fff;
        font-size: 22px;
        font-family: 'Kontora Bold';
    }

    @media only screen and (min-width: 1024px) {
        margin-bottom: 0;
        margin-right: 80px;

        a {
            color: #2e2e2e;
            font-size: 15px;
            transition: color 0.5s;
            position: relative;
            transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);

            &::before {
                content: '';
                position: absolute;
                z-index: -1;
                width: 110%;
                height: 3px;
                left: -5%;
                bottom: -3px;
                transform: ${(props) => (props.active ? 'scale3d(1, 1, 1)' : 'scale3d(0, 1, 1)')};
                transform-origin: 0% 50%;
                transition: transform 0.5s;
                background-color: #fdbc52;
                transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
            }

            &:hover {
                &::before {
                    transform: scale3d(1, 1, 1);
                }
            }
        }
    }
`;

export const MenuItemsContainer = styled.ul<{ isMenuOpen: boolean }>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
    width: 100%;
    position: fixed;
    transition: top 0.5s;
    margin: 0;
    background-color: #1b4b78;
    top: ${(props) => (props.isMenuOpen ? '0' : '-180vh')};
    left: 0;
    list-style: none;
    padding: 0;
    z-index: 4;

    @media only screen and (min-width: 1024px) {
        position: initial;
        width: initial;
        height: 100%;
        flex-direction: row;
        background-color: inherit;

        ${InfoBox} {
            display: none;
        }
    }
`;
