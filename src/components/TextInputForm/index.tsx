import React from 'react';
import { InputWrapper, Label, InputText, ErrorMessage, Textarea } from './styled';

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    error?: string;
}

const TextInputForm = React.forwardRef((props: Props, ref) => {
    return (
        <InputWrapper>
            <Label htmlFor={props.id}>{props.label}</Label>
            {props.type === 'textarea' ? (
                <Textarea {...(props as any)} ref={ref} />
            ) : (
                <InputText type="text" {...(props as any)} ref={ref} />
            )}

            {props.error && <ErrorMessage>{props.error}</ErrorMessage>}
        </InputWrapper>
    );
});

export default React.memo(TextInputForm);
