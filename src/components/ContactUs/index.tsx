import React, { useState } from 'react';
import ReactGA from 'react-ga';
import TextInputForm from '../TextInputForm';
import { ContactUsContainer, PhoneImage } from './styled';
import { useForm } from 'react-hook-form';
import { YellowButton } from '../ui';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
import { CheckboxForm } from '../CheckboxForm';
import ReactPixel from 'react-facebook-pixel';

ReactGA.initialize('UA-178462528-1');

type FormData = {
    name: string;
    email: string;
    companyName: string;
    comment: string;
    isConfirm: boolean;
};

const schema = yup.object().shape({
    name: yup.string().required('Name is a required field'),
    email: yup.string().required('Email is a required field').email('Email must be a valid'),
    companyName: yup.string().required('Company name is a valid field'),
    isConfirm: yup.boolean().test({
        test: (val) => val,
        message: 'Required',
    }),
});

export const ContactUs = () => {
    const [isLoading, setLoadingStatus] = useState<boolean>(false);
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        setLoadingStatus(true);
        fetch('https://formspree.io/mgeelwbr', {
            method: 'POST',
            mode: 'no-cors',
            body: JSON.stringify(data),
        }).then((res: Response) => {
            reset();
            console.log(res.status);

            if (res.status < 400) {
                window.alert('Your application has been sent successfully!');
                window.scrollTo({ top: 0 });
                ReactGA.event({
                    category: 'Submit contact',
                    action: 'Valid submition',
                });

                ReactPixel.trackCustom('Lead');
            } else {
                window.alert("Your request couldn't be sent...");
                window.scrollTo({ top: 0 });
            }
            setLoadingStatus(false);
        });
    });

    return (
        <ContactUsContainer id="contact-us">
            <h2>Contact Us</h2>
            <form onSubmit={onSubmit}>
                <TextInputForm
                    name="name"
                    ref={register}
                    placeholder="Your name"
                    label="Name"
                    error={errors.name?.message}
                />
                <TextInputForm
                    name="email"
                    ref={register}
                    placeholder="Your email"
                    label="Email"
                    error={errors.email?.message}
                />
                <TextInputForm
                    name="companyName"
                    ref={register}
                    placeholder="Company name"
                    label="Company name"
                    error={errors.companyName?.message}
                />
                <TextInputForm
                    name="comment"
                    type="textarea"
                    ref={register}
                    placeholder="Write your message..."
                    label="Comment"
                    error={errors.comment?.message}
                />
                <CheckboxForm
                    ref={register}
                    name="isConfirm"
                    error={errors.isConfirm?.message}
                    label="I agree to the Terms of processing of my personal data"
                />
                <YellowButton disabled={isLoading}>
                    {!isLoading ? <input type="submit" value="Submit" /> : 'Pending...'}
                </YellowButton>
            </form>
            <PhoneImage />
        </ContactUsContainer>
    );
};
