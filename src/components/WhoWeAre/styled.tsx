import styled from 'styled-components';
import { GhostButton } from '../ui';

export const WhoWeAreContainer = styled.div`
    background: #f9fbfc;
    padding: 30px 25px 50px;

    h2 {
        margin: 0 0 20px;
    }
    img {
        display: block;
        margin: 0 auto 20px;
    }
    p {
        margin-bottom: 30px;
        font-size: 16px;
        line-height: 160%;
    }
    ${GhostButton} {
        margin: 0 auto;
    }

    @media only screen and (min-width: 768px) {
        padding: 30px 45px 50px;
        position: relative;
        padding-left: 50%;
        ${GhostButton} {
            margin: 0;
        }
        img {
            position: absolute;
            left: 45px;
            top: 30px;
            margin: 0;
            max-height: 360px;
        }
    }
    @media only screen and (min-width: 1024px) {
        img {
            top: -30px;
            width: 421px;
            max-height: 518px;
        }
    }
    @media only screen and (min-width: 1440px) {
        margin-right: calc(50% - 652px);
        border-radius: 0px 70px 70px 0px;
        padding: 60px 88px 60px 50%;

        img {
            left: calc(50% - 450px);
        }
    }
`;
